#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 31 15:59:41 2020
@author: cesar
"""

import pandas as pd
import numpy as np
import sys
import itertools
from IPython.display import clear_output

def processing_data(df,time=600,n_data=50,distance=15,file=None,eta=True,chunks = 200000,large=True,file_aux='test.csv'):
    """
    Esta función toma un dataframe en bruto y le realiza las siguientes modificaciones
    1. Elimina las columnas que no aportan información al ETA
    2. Filtrar detenciones tempranas, existen vehículos que antes de empezar el viaje 
        se detienen a descanzar en la salida de la faena, esta funcion elimina esos datos, 
        solo los iniciales, este NO ES UN FILTRO DE DETENCIONES
    3. Una vez filtrados los viajes se agrega la columna de ETA, tiempo acumulado y distancia acumulada
    4. Cambiamo la hora a hora chilena y agregamos el bloque horario
    5. Redondeamos los decimales de lat y lon a 4 decimales que son los necesarios para no perder precisión
    6. guardamos el nuevo dataset.
    
    Params:
    df = dataframe
    time = tiempo máximo para ser considerado una detención falsa (pto 2) (seg)
    n_data = primeras x filas a considerar para el filtrado de detenciones
    distance = distancia max que se puede mover un vehículo para no considerarse una detencion falsa
    file =  nombre del archivo de salida
    eta =  si queremos incorporar la columna ETA
    large = si el df original es muy largo se recomienda utilizar chunks
    chunks = cantidad de chunks para manejar los datos
    file_aux = para utilizar los chunks se genera un dataset intermedio con nombre x
    """
    
    
    print("step 1: drop columns")
    
    df_new = None
    keys = ['Id','lat','lon','dt','delta_m','delta_t','Origen','Destino']
    columns = df.columns.tolist()
    drop = [x for x in columns if x not in keys]
    df = df.drop(drop, axis=1)
    
    print("step 2: delete rows of initial sleep")

    viajes = df['Id'].unique().tolist()
    delete = []
    for i in viajes:
        aux = df[df['Id']==i]
        aux = aux.iloc[:n_data,:].loc[:,['delta_m','delta_t']]
        aux = aux[(aux['delta_t']>=time)]    
        if aux.shape[0] > 1:
            aux = aux[(aux['delta_m']<= distance)]
            indexs = aux.index.tolist()
            delete.append(indexs)
    delete = list(itertools.chain.from_iterable(delete))
    print("rows deleted: ",len(delete))
    df = df.drop(delete,axis=0)
    
    print("step 3: Add new columns: time and distance")
    #para datasets grandes
    if large:
        df.to_csv(file_aux,index=False)
        df_chunk =  pd.read_csv(file_aux, chunksize=chunks)
        chunk_list = []  # append each chunk df here 
        df_concat = []
        count = 1 
        df_aux = None    
        for chunk in df_chunk:
            print(count)
            count+=1
            # perform data filtering 
            eta =True
            df_new = None
            for i in chunk.Id.unique().tolist():
                if i != chunk.Id.unique().tolist()[-1]:
                    a = chunk[chunk['Id'] == i]
                    print("dispatch ", i , "/",chunk['Id'].unique().tolist()[-1])
                    clear_output(wait=True)
                    a = a.assign( time_s = a.delta_t.cumsum(),
                                  distance_m = a.delta_m.cumsum().round(0))
                    if eta:
                        a = a.assign( ETA = a.time_s.iloc[-1] - a.time_s)
                    if i == 1:
                        df_aux = a
                    else:
                        df_aux = pd.concat([df_aux,a],axis=0,ignore_index=True)   
                else:
                    continue
            df_concat.append(df_aux)
        print('concatenando')
        # concat the list into dataframe 
        df_new = pd.concat(df_concat)

    #para datasets no tan grandes
    else:
        df_new = None
        for i in df.Id.unique().tolist():

            a = df[df['Id'] == i]
            a = a.assign( time_s = a.delta_t.cumsum(),
                          distance_m = a.delta_m.cumsum().round(0))
            if eta == True:
                a = a.assign( ETA = a.time_s.iloc[-1] - a.time_s)

            if i == 1:
                df_new = a
            else:
                df_new = pd.concat([df_new,a],axis=0)
            print("dispatch ", i)
            clear_output(wait=True)

    print("step 4: change datetime to Chile/Continental and add new column: hour")
    #Debemos convertir la fecha.
    df_new = df_new.assign(dt_Chile = pd.to_datetime(df_new['dt'], utc = True).dt.tz_convert('Chile/Continental'))
    df_new = df_new.assign(hour = pd.to_datetime(df_new['dt_Chile']).dt.hour)

    print("step 5: round to 4 decimals the features latitude and longitude")
    df_new['lat'] = df_new['lat'].round(4)
    df_new['lon'] = df_new['lon'].round(4)
    
    print('step 6: save new df as csv')
    #ordenamos las variables
    orden = ['Id','time_s', 'distance_m','hour','lat', 'lon','delta_t','delta_m']
    df_new = df_new.drop(['dt'],axis=1)
    if eta==True:
        df_new = df_new.reindex(columns=orden+['ETA'])
    else:
        df_new = df_new.reindex(columns=orden)
    #creamos el nuevo df
    if file != None:
        df_new.to_csv(file,index=False)
        
    return df_new


def processing_stops(df,limit_dist=200,limit_time=20,initial_distance=15, file=None):
    """
    En esta funcion se realiza el filtro del dataset ya pre-procesado para obtener el dataset de paradas,
    las funcionalidades que posee son las siguientes:
    1. Filtramos los momentos de detenciones
    2. se eliminan las columnas que no nos interesan para el entrenamiento
    3. se acumulan y suman las detenciones iguales, considerando que los gps emiten información cada x tiempo, 
        por lo que una parada no está solamente ligada a 1 fila sino a multiples 
    4. Guardar el dataset
    
    Params:
    
    df               = dataframe
    limit_dist       = distancia límite para filtrar los viajes que no hacen recorridos correctos [km]
    limit_time       = desde cuanto tiempo [min] se comienza a considerar detencion
    initial_distance = desde  cuantos km se comienzan a considerar las detenciones
    file             = nombre del archivo csv donde guardar el dataset 
    """
    
    #realizamos el 1er filtro de paradas dm=0 y dt!=0
    print("step 1: Filter by wait moments")
    df = df[(df['delta_t']!=0) & (df['delta_m'] <= 50) & (df['distance_m']>initial_distance*1000) & (df['distance_m']<limit_dist*1000)]
    
        #nos quedamos con las variables que nos importan, por el momento
    print("step 2: filter columns")
    var = ['Id','delta_t', 'distance_m','lat','lon','hour']
    df = df.loc[:,var]
    df = df.assign(distance_km = df['distance_m']/1000)
    df['distance_km'] = df['distance_km'].astype(int)
    
    
    print("step 3: group and sum the wait moments")
    viajes = df.Id.unique().tolist()
    df_aux = [] 
    columns = ['Id','time [min]', 'distance_km','latitude','longitude','hour']
    for i in viajes:
        aux = df[df['Id']==i]
        if aux.shape[0] == 1:
            a = [ aux['Id'].values[0], aux['delta_t'].values[0]/60, aux['distance_km'].values[0],
                  aux['lat'].values[0], aux['lon'].values[0], aux['hour'].values[0]]
            df_aux.append(a)
        else:
            distance = aux['distance_km'].unique().tolist()
            distance = rectification(distance)  #rectificamos las distancias, agrupamos las parecidas
            for i in distance:
                b = aux[(aux['distance_km']>=i-1) & (aux['distance_km']<=i+1)]
                dispatch = int(b['Id'].mode())
                time = b['delta_t'].sum()
                distance = i
                lat = b['lat'].mean()
                lon = b['lon'].mean()
                horario = int(b['hour'].iloc[0])
                
                df_aux.append([dispatch,time/60,distance,lat,lon,horario])
                
    df_final = pd.DataFrame(df_aux,columns=columns)
    df_final = df_final[df_final['time [min]']>limit_time]
    df_final = df_final.assign(wait_s = df_final['time [min]']*60)
    print("step 4: save dataframe")
    if file != None:
        df_final.to_csv(file)
    return df_final

def rectification(lista,std=1):
    for index,value in enumerate(lista):
        aux = lista[index+1:]
        for index_2,compare in enumerate(aux):
            if(value-std <= compare <= value+std):
                indice=index+index_2+1
                lista.pop(indice)
    return lista

    
def processing_dynamics(data,tiempo_limite,file=None,time_min=1800):
    """
    Esta función cumple con la tarea de crear un dataset para entrenamiento de modelos dinámicos, 
    es decir para aquellos viajes que son considerados ideales, sin mayores detenciones de por medio.
    Para ello esta función realiza las siguientes tareas:
    1. Redondea las variables de lat y lon y define las variables (columnas) a utilizar
    2. Filtra los ID de viajes que cumplan con el tiempo que se considera un viaje normal.
    3. Crea el nuevo dataset a partir de los ID seleccionados
    4. Grabando el csv

    PARAMS:
    data = dataframe
    tiempo_limite = tiempo en que un viaje es considerado normal [seg]
    file = nombre archivo csv de salida
    """
    
    print('step 1: round features')
    df =  data.copy()
    keys = ['Id','Origen','Destino','time_s','distance_m','hour','lat','lon','ETA']
    columns = df.columns.tolist()
    drop = [x for x in columns if x not in keys]
    df = df.drop(drop, axis=1)
    df['distance_m']=df['distance_m'].astype(int)
    df['lat'] = df['lat'].round(4)
    df['lon'] = df['lon'].round(4)
    
    print('step 2: select filter dispatch id')
    df_new = df.groupby(by=['Id']).last()
    viajes_normales = df_new[(df_new['time_s']<tiempo_limite) & (df_new['time_s']>time_min)].index.tolist()
    viajes_anormales = df_new[df_new['time_s']>tiempo_limite].index.tolist()
    print("# de viajes normales: ",len(viajes_normales))


    print('step 3: create the new DataFrame')  
    df_new = data.loc[df['Id'].isin(viajes_normales)]
    
    
    if file != None:
        print('step 4: Saving file')
        df_new.to_csv(file,index=False)
    return df_new