from sagemaker.amazon.amazon_estimator import get_image_uri
import boto3
import time

runtime = boto3.Session().client('runtime.sagemaker')

def deploy_own_model(name, name_targz ,role, bucket, prefix, model_type,instance_type = 'ml.m4.xlarge',version='0.90-1'):
    print('creacion de nombres')
    """Creacion de nombres"""
    date = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime())
    name_model =  name + date # nombre del modelo
    endpoint_config = name + '-endpoint-config-' + date  # nombre configuracion
    name_endpoint = name + '-endpoint-' + date #nombre endpoint

    """Creacion del host para el endpoint"""
    sm = boto3.client('sagemaker') #indicamos el host
    if model_type == 'xgboost':
        container = get_image_uri(boto3.Session().region_name, 'xgboost', version)
    else:
        container = get_image_uri(boto3.Session().region_name, model_type) #importamos el modelo

    print('construimos el modelo')
    #construimos el modelo para Sage Maker
    create_model = sm.create_model(
        ModelName=name_model,
        ExecutionRoleArn=role,
        PrimaryContainer={
            'Image': container,
            'ModelDataUrl': 's3://{}/{}/{}'.format(bucket, prefix,name_targz)})
    print('direccion del modelo: s3://{}/{}/{}'.format(bucket, prefix,name_targz))
    print(create_model['ModelArn'])    
    print(endpoint_config)
    print('configuración del endpoint')
    """Configuracion del endpoint"""
    create_endpoint_config = sm.create_endpoint_config(
        EndpointConfigName = endpoint_config,
        ProductionVariants = [{
            'InstanceType': instance_type,
            'InitialInstanceCount': 1,
            'ModelName': name_model,
            'VariantName': 'AllTraffic'}])

    print("Endpoint Config Arn: " + create_endpoint_config['EndpointConfigArn'])


    """Creacion del endpoint"""
    print(name_endpoint)
    print('creacion del endpoint')

    #Creamos el EndPoint
    create_endpoint_response = sm.create_endpoint(
        EndpointName = name_endpoint,
        EndpointConfigName = endpoint_config)
    print(create_endpoint_response['EndpointArn'])


    resp = sm.describe_endpoint(EndpointName = name_endpoint)
    status = resp['EndpointStatus']
    print("Status: " + status)

    sm.get_waiter('endpoint_in_service').wait(EndpointName = name_endpoint, WaiterConfig={
        'Delay': 30,
        'MaxAttempts': 20
    })

    resp = sm.describe_endpoint(EndpointName = name_endpoint)
    status = resp['EndpointStatus']
    print("Arn: " + resp['EndpointArn'])
    print("Status: " + status)

    if status != 'InService':
        raise Exception('Endpoint creation did not succeed')
    return name_endpoint

def inferencia(endpoint , input_data):
    response = runtime.invoke_endpoint(EndpointName=endpoint, ContentType='text/csv', Accept='text/csv', Body=input_data)
    prediction = response['Body'].read().decode("utf-8")
    return prediction